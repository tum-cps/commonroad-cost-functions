# Cost Functions of CommonRoad

This repository contains all cost functions of the [CommonRoad benchmarks](https://commonroad.in.tum.de/).

We provide a documentation of all available cost functions.

# Python Implementation

We provide a Python implementation to evaluate cost functions on CommonRoad benchmarks in the [Drivability Checker](https://commonroad.in.tum.de/drivability-checker).

## Contribute

If you want to contribute new cost functions, you can contact us per [e-mail](mailto:commonroad-i06@in.tum.de).

## References

If you use CommonRoad, please cite *[M. Althoff, M. Koschi, and S. Manzinger, ''CommonRoad: Composable Benchmarks for Motion Planning on Roads,'' in Proc. of the IEEE Intelligent Vehicles Symposium, 2017, pp. 719-726](http://mediatum.ub.tum.de/doc/1379638/776321.pdf)*.
